# UserAPI

A simple api to create users

## Endpoints

- `GET /users/` : Lists all the users
- `POST /users/create/` : Creates a user specified in the body
