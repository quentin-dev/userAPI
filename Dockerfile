FROM python:3.9.0-alpine AS builder

ENV PYTHONUNBUFFERED 1

WORKDIR /usr/src/userapi

RUN apk update
RUN apk add curl

RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -

COPY poetry.lock .
COPY pyproject.toml .

RUN source $HOME/.poetry/env && poetry export -f requirements.txt --output requirements.txt

FROM python:3.9.0-alpine

ENV PYTHONUNBUFFERED 1

WORKDIR /usr/src/userapi

RUN apk update
RUN apk add libffi-dev
RUN apk add git postgresql-dev gcc python3-dev musl-dev

RUN pip install --no-cache-dir uvicorn

COPY --from=builder /usr/src/userapi/requirements.txt .

RUN pip install --no-cache-dir -r requirements.txt

COPY . .

CMD ["uvicorn", "userapi.main:app", "--host", "0.0.0.0", "--port", "8000"]
