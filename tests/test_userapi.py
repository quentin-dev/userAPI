import os

import pytest
from fastapi.testclient import TestClient

from userapi import __version__
from userapi.main import app, get_db

from .mock_db_provider import override_get_db

app.dependency_overrides[get_db] = override_get_db
client = TestClient(app)


def remove_test_db():
    if os.path.exists("userapitest.db"):
        os.remove("userapitest.db")


@pytest.fixture(scope="session", autouse=True)
def remove_test_db_after_user(request):
    request.addfinalizer(remove_test_db)


def test_version():
    assert __version__ == "0.1.0"


def test_create_user():
    response = client.post("/users/create", json={"email": "test@test.fr"})

    assert response.status_code == 201


def test_create_duplicate_user():
    response = client.post("/users/create", json={"email": "test@testing.fr"})

    assert response.status_code == 201

    response = client.post("/users/create", json={"email": "test@testing.fr"})

    assert response.status_code == 409
