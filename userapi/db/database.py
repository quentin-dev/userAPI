import os

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


def get_postgres_url():

    user = os.getenv("POSTGRES_USER", "postgres")
    password = os.getenv("POSTGRES_PASSWORD", "postgres")
    server = os.getenv("POSTGRES_SERVER", "db")
    db = os.getenv("POSTGRES_DB", "postgres")

    return os.getenv("DATABASE_URL", f"postgresql://{user}:{password}@{server}/{db}")


SQLALCHEMY_DATABASE_URL = (
    get_postgres_url()
    if ("POSTGRES_DB" in os.environ) or ("DATABASE_URL" in os.environ)
    else "sqlite:///./userapi.db"
)


engine = create_engine(SQLALCHEMY_DATABASE_URL)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()
