from fastapi import FastAPI, HTTPException
from fastapi.params import Depends
from sqlalchemy.orm.session import Session

from .api import schemas
from .db import crud, database, models

models.Base.metadata.create_all(bind=database.engine)

app = FastAPI()


# Dependency
def get_db():
    db = database.SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.post("/users/create", response_model=schemas.User, status_code=201)
def create_user(user: schemas.UserBase, db: Session = Depends(get_db)):
    db_user = crud.get_user_by_email(db, email=user.email)
    if db_user is not None:
        raise HTTPException(status_code=409, detail="Email already registered")
    return crud.create_user(db=db, user=user)
